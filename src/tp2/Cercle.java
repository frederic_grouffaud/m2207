package tp2;

public class Cercle extends Forme  {
	//Attributs
	private double rayon;


	//Constructeurs
	Cercle (){
		super();
		rayon = 1.0;
	}

	Cercle(double r){
		r= 1.0;
	}

	Cercle(double r, String couleur, boolean coloriage){
		super();

	}


	//Accesseurs
	void setRayon(double r) {
		rayon= r;
	}
	double getRayon() {
		return rayon;
	}




	//Mthodes
	String seDecrire(){
		return "Un cercle de rayon"+" "+rayon+" "+"est issue d'une Forme de couleur"+" "+getCouleur()+" "+"et de coloriage"+" "+isColoriage(); 
	}

	double calculerAire() {
		return Math.PI*rayon*rayon;
	}
	
	double calculerPerimetre() {
		return Math.PI*2*rayon;
	}

	
}
