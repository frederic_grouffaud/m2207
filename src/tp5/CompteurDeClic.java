package tp5;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout; 

import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener{
	
	//Attributs
	int c;
	JButton a = new JButton("Click !");
	JLabel nombreClick = new JLabel("Vous avez clické 0 fois");
	
	
	//Constructeurs
	
	public CompteurDeClic () 
	{
		super();
		this.setTitle("Compteur de clicks");
	    this.setSize(400,400) ;
		this.setLocation(100,100) ;
		this.setVisible(true) ;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		this.setVisible(true);
		System.out.println("La fenetre est crée !");
	
		
		Container panneau = getContentPane();
		panneau.setLayout(new GridLayout(3,2)) ;
		panneau.add(a, BorderLayout.CENTER);
		panneau.add(nombreClick, BorderLayout.CENTER);
		a.addActionListener(this);
		
		
	}
	
	//Méthodes Main
	
	public static void main(String[] args) 
	{
		CompteurDeClic app = new CompteurDeClic ();
	}

	public void actionPerformed1(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void actionPerformed(ActionEvent e) {
		System.out.println("Une action a t detecte");
		c=c+1;
		nombreClick.setText("Vous avez cliqué "+c+ " fois");
	}

}
